/*
 * ask.h
 *
 *  Created on: Apr 17, 2016
 *      Author: roland
 */

#ifndef ASK_H_
#define ASK_H_

#include <stm32f10x.h>

class Ask
{
public:
    Ask(USART_TypeDef* uart, uint16_t baudrate);
    void send(uint8_t data);
    uint8_t receive(uint8_t* data, uint8_t bufLen);

//private:
    void sendStart();
    void sendStop();
    uint8_t rcvByte();
    bool convertToByte(uint16_t data, uint8_t& out);

    USART_TypeDef* _uart;
};



#endif /* ASK_H_ */
