/*
 * ask.cpp
 *
 *  Created on: Apr 17, 2016
 *      Author: roland
 */

#include "ask.h"

Ask::Ask(USART_TypeDef* uart, uint16_t baudrate)
    : _uart(uart)
{
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);

    GPIO_InitTypeDef gpio;
    gpio.GPIO_Speed = GPIO_Speed_50MHz;
    gpio.GPIO_Mode = GPIO_Mode_AF_PP;
    gpio.GPIO_Pin = GPIO_Pin_2;
    GPIO_Init(GPIOA, &gpio);

    gpio.GPIO_Mode = GPIO_Mode_IN_FLOATING;
    gpio.GPIO_Pin = GPIO_Pin_3;
    GPIO_Init(GPIOA, &gpio);

    USART_InitTypeDef usart;
//    usart.USART_BaudRate = baudrate;
//    usart.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
//    usart.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
//    usart.USART_Parity = USART_Parity_No;
//    usart.USART_StopBits = USART_StopBits_1;
//    usart.USART_WordLength = USART_WordLength_8b;
    USART_StructInit(&usart);
    usart.USART_BaudRate = baudrate;
    USART_Init(USART2, &usart);
    USART_Cmd(USART2, ENABLE);
}

void Ask::send(uint8_t data)
{
    uint8_t bytes[2] = {0, 0};

    for(uint8_t bitNum = 0; bitNum < 8; ++bitNum)
    {
        uint8_t byteNum = bitNum / 4;
        if(data & (1 << bitNum))
        {
            bytes[byteNum] |= 0b01 << ((bitNum * 2) % 8);
        }
        else
        {
            bytes[byteNum] |= 0b10 << ((bitNum * 2) % 8);
        }
    }

    for(uint8_t i = 0; i < 2; ++i)
    {
        USART_SendData(_uart, bytes[i]);
        while(!USART_GetFlagStatus(_uart, USART_FLAG_TXE))
            ;
    }
}

uint8_t Ask::receive(uint8_t* data, uint8_t bufLen)
{
    bool success = false;
    uint8_t pos = 0;

    do
    {
        uint8_t rcv = 0;
        do
        {
            rcv = rcvByte();
        }while(rcv != 0xf0);

        for(pos = 0; pos < bufLen; ++pos)
        {
            rcv = rcvByte();

            if(rcv == 0x0f)
                break;

            if(rcv == 0xf0)
            {
                success = false;
                pos = 0;
                continue;
            }

            uint16_t twoBytes = rcv;

            rcv = rcvByte();

            if(rcv == 0x0f)
            {
                success = false;
                break;
            }

            if(rcv == 0xf0)
            {
                success = false;
                pos = 0;
                continue;
            }

            twoBytes |= rcv << 8;

            success = convertToByte(twoBytes, data[pos]);
            if(!success)
                break;
        }

    }while(!success);

    return pos;
}

void Ask::sendStart()
{
    USART_SendData(_uart, 0xf0);
    while(!USART_GetFlagStatus(_uart, USART_FLAG_TXE))
        ;
}

void Ask::sendStop()
{
    USART_SendData(_uart, 0x0f);
    while(!USART_GetFlagStatus(_uart, USART_FLAG_TXE))
        ;
}

uint8_t Ask::rcvByte()
{
    while(!USART_GetFlagStatus(_uart, USART_FLAG_RXNE))
        ;

    return USART_ReceiveData(_uart);
}

bool Ask::convertToByte(uint16_t data, uint8_t& out)
{
    for(uint8_t i = 0; i < 8; ++i)
    {
        if((data & 0x3) == 0b01)
            out |= 1 << i;
        else if((data & 0x03) == 0b10)
            out &= ~(1 << i);
        else
            return false;

        data >>= 2;
    }

    return true;
}

